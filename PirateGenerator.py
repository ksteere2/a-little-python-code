# -------------------------------------------------------------------------------
# Name:        Pirate Generator
# Purpose:     To generate pirates...
# Author:      steerek
# Created:     28/05/2014
# ------------------------------------------------------------------------------
"""
  PIRATE ATTRIBUTES:
    Name = the pirate's name.
    weapons = selects what weapons the pirate prefers.
    wielding = specifies if the character is a dual wielder.
    type = class type such as Captain or berserker.
    Bounty = a higher bounty implies a more notorious character.
    origin = the general area the pirate is from.
    Notoriety = how notorious is this pirate is.
"""
import random

#Weapons, Types, Origins lists
weaponList = ['cutlass', 'pistol', 'meat hook', 'club', 'dagger', 'axe','blunderbuss', 'meat cleaver']
classList = ['Captain', 'crew mate', 'saboteur', 'pillager', 'swindler', 'First mate', 'privateer']
originsList = ['the Bahamas', ' the Indian Sea', 'Honduras', 'Spain', 'the Mediterranean', 'Morocco' ,'West Indies']

#Names lists
FNameList = ['Stinky', 'Jolly', 'Monkey', 'Bloody', 'Crabby', 'Black', 'Red', 'Rusty', 'Squirmy', 'Mad', 'Johnny',
             'Rat', 'Kidd', 'Gory', 'Dread', 'Gold', 'Rotting', 'Squid', 'Stubby']
LNameList = ['Lash', 'Beard', 'Monkey', 'Eyes', 'Shanks', 'Babble', 'Morgan', "O' Molly", 'Jackson', 'Swaggle',
             'Boots', 'Robin', 'Jones', 'Dog', 'Rivers']

#Pirate generation functions.
def genName():
    firstName = random.choice(FNameList)
    lastName = random.choice(LNameList)
    return firstName + ' ' + lastName

def wielding():
    return random.choice([True, False])

def weaponSelect():
    return random.choice(weaponList)

def selectClass():
    return random.choice(classList)

def location():
    return random.choice(originsList)

def bounty():
    if pirateClass == 'Captain':
        return random.randint(750, 1000)
    elif pirateClass == 'First mate':
        return random.randint(500, 1000)
    elif pirateClass == 'saboture' or 'pillager' or 'swindler' or 'privateer':
        return random.randint(50, 1000)
    elif pirateClass == 'crew mate':
        return random.randint(1, 50)
    else:
        return 1

def notoriety():
    if pirateBounty >= 990:
        return "is a Legendary Horror, striking fear into all!"
    elif 900 <= pirateBounty <= 990:
        return "terrifies the common folk!"
    elif 800 <= pirateBounty <= 900:
        return "is freak'n scary and considered very dangerous."
    elif 700 <= pirateBounty <= 800:
        return "frightens many villagers."
    elif 600 <= pirateBounty <= 700:
        return "is fairly notorious, and considered dangerous."
    elif 500 <= pirateBounty <= 600:
        return "is not very notorious."
    elif 400 <= pirateBounty <= 500:
        return "doesn't scare anyone."
    elif 300 <= pirateBounty <= 400:
        return "is mostly a joke."
    elif 200 <= pirateBounty <= 300:
        return "is just a stole away."
    else:
        return "couldn't make a baby cry."

#Generate the pirate specs
pirateName = genName()
pirateLoc = location()
pirateWield = wielding()
pirateWep1 = weaponSelect()
pirateWep2 = weaponSelect()
pirateClass = selectClass()
pirateBounty = bounty()
pirateNotoriety = notoriety()

#Display the results.
print("Your pirate's name is %s, and has a bounty of %s pounds.") % (pirateName, pirateBounty)
print("This pirate %s") % pirateNotoriety
if pirateWield is True and pirateWep1 == pirateWep2:
    print("Your pirate dual wields a pair of %ss." % pirateWep1)
elif pirateWield is True and pirateWep1 != pirateWep2:
    print("Your pirate is a dual weapons wielder, fighting with a %s in one hand and a %s in the other." % (
        pirateWep1, pirateWep2))
elif pirateWield is False:
    print("Your pirate's weapon of choice is a %s." % pirateWep1)
else:
    print"none"

print("%s is a pirate %s, who hales from %s." % (pirateName, pirateClass, pirateLoc))


